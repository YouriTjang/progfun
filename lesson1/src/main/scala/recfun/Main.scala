package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int =
    if (c == 0 || c >= r) {
      1
    } else if (c == 1 || c == r - 1) {
      r
    } else {
      pascal(c, r - 1) + pascal(c - 1, r - 1)
    }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {

    def convertParen( char: Char) : Int = char match {
      case '(' =>  1
      case ')' => -1
      case  _  =>  0
    }

    def b(chars: List[Char], parens: Int): Boolean = {
      if( chars.isEmpty)
        parens == 0
      else if (parens < 0)
        false
      else
        b(chars.tail, parens + convertParen(chars.head) )
    }

    b(chars, 0)
  }
  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    if ( money < 0 || coins.isEmpty)
      0
    else if ( money == 0)
      1
    else {
      countChange(money - coins.head, coins) + countChange(money, coins.tail)
    }
  }
}
