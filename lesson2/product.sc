object exercise {
  def product(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 1
    else f(a) * product(f)(a+1, b)

  product(x => x * x)(3,4)

  def fact(a: Int) = product(x => x)(1, a)

  fact(5)

  def reduce(f: Int => Int)(op: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
    if (a > b) zero
    else op(f(a),  (reduce(f)(op, zero)(a+1, b)))


}
