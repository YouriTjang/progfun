object rational {
  val x = new Rational(1, 3)
  val y = new Rational(5, 7)
  val z = new Rational(3, 2)

  x.numer
  x.denom
  x - y  - y
  y + y
  x< y
  x max y

//  val strange = new Rational(1, 0)
//  strange.+(strange)
}

class Rational(x: Int, y: Int) {
  require(y != 0, "denominator must be nonzero")

  private def gcd(a: Int, b: Int): Int =
    if (b == 0 ) a else gcd(b, a % b)

  private val g = gcd(x, y)

  def numer = x / g
  def denom = y / g

  def + (that: Rational): Rational = {
    new Rational(numer * that.denom + that.numer * denom, denom + that.denom)
  }

  def - (that: Rational): Rational = {
    this + -that
  }

  def unary_- : Rational = {
    new Rational(-numer, denom)
  }

  def < (that: Rational): Boolean =
    numer * that.denom < that.numer * denom

  def max(that: Rational): Rational = {
    if ( this < that) that else this

  }
  override
  def toString: String = {
    numer/g + "/" + denom/g
  }
}